package com.preemynence.learnigSOLRj.dao;

import lombok.Data;
import org.apache.solr.client.solrj.beans.Field;

@Data
public class ProductBean {
	@Field("id")
	private String id;
	@Field("name")
	private String name;
	@Field("price")
	private String price;
}