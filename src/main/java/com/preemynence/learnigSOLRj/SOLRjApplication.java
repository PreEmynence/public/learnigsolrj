package com.preemynence.learnigSOLRj;

import com.preemynence.learnigSOLRj.sevice.SolrJavaIntegration;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import java.io.IOException;

public class SOLRjApplication {
	
	public static void main(String args[]) {
		
		SOLRjApplication app = new SOLRjApplication();
		SolrJavaIntegration solrJavaIntegration = new SolrJavaIntegration("http://localhost:8983/solr/bigboxstore");
		app.testAdd(solrJavaIntegration);
		app.verifyAdd(solrJavaIntegration);
	}
	
	private void verifyAdd(SolrJavaIntegration solrJavaIntegration) {
		try {
			SolrQuery query = new SolrQuery();
			query.set("q", "id:123456");
			QueryResponse response = null;
			
			response = solrJavaIntegration.getSolrClient().query(query);
			
			SolrDocumentList docList = response.getResults();
			if (1 == docList.getNumFound()) {
				for (SolrDocument doc : docList) {
					if ("Kenmore Dishwasher".equals(doc.getFieldValue("name"))) {
						if (599.99f == Float.parseFloat(doc.getFieldValue("price").toString())) {
							System.out.println("Got the record.");
						}
					}
				}
			}
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void testAdd(SolrJavaIntegration solrJavaIntegration) {
		try {
			
			solrJavaIntegration.addSolrDocument("123456", "Kenmore Dishwasher", "599.99");
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
